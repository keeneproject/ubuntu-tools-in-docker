# ubuntu-tools-in-docker

# What is this
This dockerfile builds an ubuntu image which contains everything required to build and maintain a Kubernetes cluster.

# How do I build it
```bash
# Build
docker build -t helm-deployer:latest .

# Generate SBOM and vulnerability report
trivy image --scanners vuln --format cyclonedx helm-deployer:latest --output helm-deployer-SBOM-VULN.json

# Syft SBOM  (syft gives a nice and simple report)
syft helm-deployer:latest > helm-deployer-SBOM.txt

# Trivy scan for vulnerabilties
trivy image helm-deployer:latest -o simple-can.txt   #visual table

# All in one SBOM+VULN use trivy
trivy image --scanners vuln --format cyclonedx helm-deployer:latest --output helm-deployer-SBOM-VULN.json

# save it to zip file. so you can scp it somewhere
docker save helm-deployer:latest | gzip > ./helm-deployer-docker-image.tar.gz 
```

# How do i run it on my air-gapped environment
```bash
# Scp it to your air-gap

# Load it on the other side
docker load < ./helm-deployer-docker-image.tar.gz

# run your container mounting our hosts home dir
docker run -d -t -v ~/:/host-home --name helm-deployer helm-deployer:latest
docker exec -it helm-deployer bash
# You might need to sudo to root to access files on the hosts home dir.

# Stop and delete the container
docker rm -f helm-deployer
```
