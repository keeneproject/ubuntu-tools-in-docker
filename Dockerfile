FROM ubuntu:24.04

# Install from apt
RUN apt-get update \
&& apt-get -y upgrade \
&& apt-get -y install curl git gzip unzip vim wget jq skopeo dnsutils lsb-release apt-utils sudo \
&& rm -rf /var/lib/apt/lists/*

# Define working directory.
WORKDIR /tmp/installationfiles/

####### Maybe use brew to get latest binaries
# Install terraform
# https://developer.hashicorp.com/terraform/tutorials/aws-get-started/install-cli
RUN wget -O- https://apt.releases.hashicorp.com/gpg | gpg --dearmor | tee /usr/share/keyrings/hashicorp-archive-keyring.gpg \
&& echo "deb [signed-by=/usr/share/keyrings/hashicorp-archive-keyring.gpg] https://apt.releases.hashicorp.com $(lsb_release -cs) main" | tee /etc/apt/sources.list.d/hashicorp.list \
&& apt-get update && apt-get install terraform \
&& rm -rf /var/lib/apt/lists/* \
&& rm -rf /tmp/installationfiles/*

# Install aws-cli
# https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html
RUN curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip" \
&& unzip awscliv2.zip \
&& ./aws/install --update \
&& rm -rf /tmp/installationfiles/*

# Install kubectl
# https://kubernetes.io/docs/tasks/tools/install-kubectl-linux/
RUN curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl" \
&& install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl \
&& rm -rf /tmp/installationfiles/*

# Install helm and helm file
# https://helm.sh/docs/intro/install/
RUN curl https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3 | bash \
&& wget https://github.com/helmfile/helmfile/releases/download/v0.153.1/helmfile_0.153.1_linux_amd64.tar.gz \
&& tar zxvf helmfile_0.153.1_linux_amd64.tar.gz \
&& chmod +x helmfile \
&& mv helmfile /usr/local/bin \
&& rm -rf /tmp/installationfiles/*

# Install k9s
# https://github.com/derailed/k9s/releases
RUN wget https://github.com/derailed/k9s/releases/download/v0.27.3/k9s_Linux_amd64.tar.gz \
&& tar -zxvf k9s_Linux_amd64.tar.gz \
&& mv k9s /usr/local/bin/k9s \
&& rm -rf /tmp/installationfiles/*

# Install trivy for SBOM and vulnerabilty scanning
RUN curl -sfL https://raw.githubusercontent.com/aquasecurity/trivy/main/contrib/install.sh | sh -s -- -b /usr/local/bin

# Install syft #for generating sbom
RUN curl -sSfL https://raw.githubusercontent.com/anchore/syft/main/install.sh | sh -s -- -b /usr/local/bin

# Add files.
#ADD ../scripts /tmp/installationfiles/

# Create non-root user with sudo access
RUN groupadd -r -g 1000 ubuntu
RUN useradd -rm -d /home/ubuntu -s /bin/bash -g ubuntu -G sudo -u 1000 ubuntu
RUN echo '%sudo ALL=(ALL) NOPASSWD:ALL' >> /etc/sudoers

# Change to non root user and set work dir
USER ubuntu
WORKDIR /home/ubuntu

######## UBUNUTU USER CONFIG

# Enable kubectl auto completion
RUN echo 'source <(kubectl completion bash)' >>~/.bashrc

# Install helm plugins
RUN yes y | helmfile init

# Scan myself for vulns
RUN trivy filesystem /
# RUN trivy fs / --download-db-only

# Define default command.
CMD ["bash"]
